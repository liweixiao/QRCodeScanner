# QRCodeScanner

#### 项目介绍
- 项目名称：实现二维码扫码功能
- 所属系列：openharmony的第三方组件适配移植
- 功能：点击跳转到扫码界面，返回扫码结果
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本： Release Tags 0.1.2

#### 效果演示
<img src="img/qrcode.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:qrcodescanner:1.0.1')
    ......  
 }
```

#### 使用说明

组件主要由QrCodeAbilitySlice实现扫码和QrManager获取结果组成。

添加相机权限，在MainAbilitySlice中执行以下方法:

//判断是否有相机权限
if (verifySelfPermission("ohos.permission.CAMERA") != IBundleManager.PERMISSION_GRANTED) {
      // 应用未被授予权限，判断是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
      if (canRequestPermission("ohos.permission.CAMERA")) {
                //申请相机权限弹框
                requestPermissionsFromUser(new String[] {"ohos.permission.CAMERA"} , 1);
      } else {
                // 显示应用需要权限的理由，提示用户进入设置授权
      }
} else {
      // 权限已被授予
}
        
QrManager.getInstance().startScan(this, new QrManager.OnScanResultCallback() {

                @Override
                public void onScanSuccess(String result) {
                    getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            // result为扫描结果
                        }
                    });
                }
            });

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.1

#### 版权和许可信息
GPLv3